<?php
/**
 * The Footer widget areas.
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */
?>

<?php
/* The footer widget area is triggered if any of the areas
 * have widgets. So let's check that first.
 *
 * If none of the sidebars have widgets, then let's bail early.
 */
if (!is_active_sidebar('first-footer-widget-area')
    && !is_active_sidebar('second-footer-widget-area')
    && !is_active_sidebar('third-footer-widget-area')
    && !is_active_sidebar('fourth-footer-widget-area')
)
    return;
// If we get this far, we have widgets. Let do this.
?>

<!--			<div id="Footer-Widgets" role="complementary">-->
<div class="Footer-Left-Wrap">
    <?php if (is_active_sidebar('first-footer-widget-area')) : ?>
        <div id="first" class="Footer-Info">
            <ul class="Contact-Info">
                <?php dynamic_sidebar('first-footer-widget-area'); ?>
            </ul>
        </div><!-- #first .widget-area -->
    <?php endif; ?>
</div>
<div class="Footer-Right-Wrap">
    <div class="Content-Right">

    <?php if (is_active_sidebar('second-footer-widget-area')) : ?>
        <div id="second" class="Footer-Info">
            <ul class="Socials-Info">
                <?php dynamic_sidebar('second-footer-widget-area'); ?>
            </ul>
        </div><!-- #second .widget-area -->
    <?php endif; ?>

    <?php if (is_active_sidebar('fourth-footer-widget-area')) : ?>
        <div id="fourth" class="Footer-Search">
            <ul class="Search">
                <?php dynamic_sidebar('fourth-footer-widget-area'); ?>
            </ul>
        </div><!-- #fourth .widget-area -->
    <?php endif; ?>

    <?php if (is_active_sidebar('third-footer-widget-area')) : ?>
        <div id="third" class="Footer-Info">
            <ul class="Language-Select">
                <?php dynamic_sidebar('third-footer-widget-area'); ?>
            </ul>
        </div><!-- #third .widget-area -->
    <?php endif; ?>
</div>
</div>


<!--			</div>-->
<!-- #footer-widget-area -->
