<?php
/**
 * The template for displaying Category Blog pages.
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */

get_header(); ?>
    <?php get_sidebar('header'); ?>

        <div class="Category Page-Blog Content-Left">
            <div class="Blog-Sidebar">
<!--                --><?php //get_sidebar(); ?>
            </div>

            
            <div class="Blog-Posts" role="main">
               <?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
                    <div class="Post-Content Massonry"  id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                            <a class="Title-Wrap" href="<?php echo get_permalink(); ?>"><h1 class="Title" >
                            <?php the_title(); ?>
                            </h1></a>
                        <div class="Descriptions">
                            <?php the_content(); ?>
                            <?php wp_link_pages( array( 'before' => '<div class="page-link">' . __( 'Pages:', 'twentyten' ), 'after' => '</div>' ) ); ?>
                        </div>
                    </div>
                <?php endwhile; ?>
            </div>

            <?php if (  $wp_query->max_num_pages > 1 ) : ?>
                <div class="paging">
                    <?php next_posts_link(); ?>
                    <?php previous_posts_link(); ?>
                </div>
<?php endif; ?>

        </div>
<?php get_footer(); ?>
