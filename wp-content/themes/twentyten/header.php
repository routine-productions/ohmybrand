<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo('charset'); ?>"/>
    <meta http-equiv="X-UA-Compatible" content="IE=9"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>
        <?php
        /*
         * Print the <title> tag based on what is being viewed.
         */
        global $page, $paged;

        wp_title('|', true, 'right');

        // Add the blog name.
        bloginfo('name');

        // Add the blog description for the home/front page.
        $site_description = get_bloginfo('description', 'display');
        if ($site_description && (is_home() || is_front_page()))
            echo " | $site_description";

        // Add a page number if necessary:
        if ($paged >= 2 || $page >= 2)
            echo ' | ' . sprintf(__('Page %s', 'twentyten'), max($paged, $page));

        //if ( is_home() || is_front_page() )
        //echo "Ohmybrand - студия брендинга. Разработка бренда, дизайна упаковки, названия бренда, идеи позиционирования, креативной концепции.";

        ?>
    </title>
    <link rel="profile" href="http://gmpg.org/xfn/11"/>
    <link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo('stylesheet_url'); ?>"/>
    <link rel="stylesheet" type="text/css" media="all" href="/css/index.min.css"/>
    <link type="image/x-icon" href="/favicon.ico" rel="SHORTCUT ICON"/>
    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>"/>
    <?php if (is_page('contacts')) { ?>
        <link href="http://code.google.com/apis/maps/documentation/javascript/examples/standard.css" rel="stylesheet"
              type="text/css"/>
    <?php } ?>
    <link href='https://fonts.googleapis.com/css?family=PT+Sans:400,700,400italic,700italic&subset=latin,cyrillic'
          rel='stylesheet' type='text/css'>
    <?php
    /* We add some JavaScript to pages with the comment form
     * to support sites with threaded comments (when in use).
     */
    if (is_singular() && get_option('thread_comments'))
        wp_enqueue_script('comment-reply');

    /* Always have wp_head() just before the closing </head>
     * tag of your theme, or you will break many plugins, which
     * generally use this hook to add elements to <head> such
     * as styles, scripts, and meta tags.
     */
    wp_head();
    ?>
    <link href="/wp-content/themes/twentyten/responsive.css" rel="stylesheet">

</head>

<body <?php body_class(); ?> onload="initialize()">
<?php
require __DIR__ . '/../../../sprite.svg';

?>
<div class="Site-Header-Wrap">
    <div class="Site-Header">
        <div id="Branding" role="banner">
            <?php if (!is_front_page()) { ?>
                <a title="На главную" href="/">
                    <svg class="Logo">
                        <use xlink:href="#logo"></use>
                    </svg>
                </a>
            <?php } else { ?>
                <svg class="Logo">
                    <use xlink:href="#logo"></use>
                </svg>
            <?php }
            ?>

        </div>

        <div class="Header-Nav-Wrap" role="navigation">
            <div class="Header-Nav">
                <?php /* Our navigation menu. If one isn't filled out, wp_nav_menu falls back to wp_page_menu. The menu assiged to the primary position is the one used. If none is assigned, the menu with the lowest ID is used. */ ?>
                <?php wp_nav_menu(array('container_class' => 'menu-header', 'theme_location' => 'primary')); ?>
                <?php wp_nav_menu(array('container_class' => 'Menu-Blog', 'theme_location' => '__no_such_location', 'menu' => '44')); ?>
            </div>
        </div>
        <?php get_sidebar('header'); ?>
    </div>
</div>

<div class="Main-Wrap">

    <?php dynamic_sidebar('top-header'); ?>


    <?php if (is_home()) { ?>
        <div class="Main-Slider Js-Slider">
            <?php echo do_shortcode("[metaslider id=4554]"); ?>
        </div>
    <?php } ?>



    <div class="Main">
        <?php get_sidebar('heads'); ?>

