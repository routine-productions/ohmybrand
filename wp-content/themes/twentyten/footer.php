<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the id=main div and all content
 * after. Calls sidebar-footer.php for bottom widgets.
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */
?>

</div><!-- #main -->

</div>
</div><!-- #wrapper -->

<div class="Site-Footer-Wrap" role="contentinfo">
    <div class="Site-Footer">
        <!--        <div class="Left-Content">-->
        <!--            --><?php //if (!is_front_page()) { ?>
        <!--                <a href="/" id="f_logo">-->
        <!--                    <img src="/wp-content/themes/twentyten/images/logo_white.png" alt="oh my brand"/>-->
        <!--                </a>-->
        <!--            --><?php //} else { ?>
        <!--                <img id="ff_logo" src="/wp-content/themes/twentyten/images/logo_white.png" alt="oh my brand"/>-->
        <!--            --><?php //} ?>
        <!--        </div>-->

        <?php
        /* A sidebar in the footer? Yep. You can can customize
         * your footer with four columns of widgets.
         */
        get_sidebar('footer');
        ?>


    </div>
    <!-- #colophon -->

</div><!-- #footer -->
<script>
    (function (i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r;
        i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date();
        a = s.createElement(o),
            m = s.getElementsByTagName(o)[0];
        a.async = 1;
        a.src = g;
        m.parentNode.insertBefore(a, m)
    })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

    ga('create', 'UA-40443108-1', 'ohmybrand.ru');
    ga('send', 'pageview');

</script>


<!-- Yandex.Metrika counter -->
<script type="text/javascript">
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function () {
            try {
                w.yaCounter21202321 = new Ya.Metrika({
                    id: 21202321,
                    clickmap: true,
                    trackLinks: true,
                    accurateTrackBounce: true
                });
            } catch (e) {
            }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () {
                n.parentNode.insertBefore(s, n);
            };
        s.type = "text/javascript";
        s.async = true;
        s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else {
            f();
        }
    })(document, window, "yandex_metrika_callbacks");
</script>
<noscript>
    <div><img src="//mc.yandex.ru/watch/21202321" style="position:absolute; left:-9999px;" alt=""/></div>
</noscript>
<!-- /Yandex.Metrika counter -->


<?php
/* Always have wp_footer() just before the closing </body>
 * tag of your theme, or you will break many plugins, which
 * generally use this hook to reference JavaScript files.
 */

wp_footer();
?>
<?php if (is_page('contacts')) { ?>
    <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
    <script type="text/javascript" src="/wp-content/themes/twentyten/js/gmap.js"></script>

<?php } ?>

<!--<script type="text/javascript" src="/wp-content/themes/twentyten/js/04-js-actual-min.js"></script>-->
<!--<script type="text/javascript" src="/wp-content/themes/twentyten/js/04-js-image-align.js"></script>-->
<script type="text/javascript" src="/wp-content/themes/twentyten/js/04-menu-fixed-js.js"></script>
<script type="text/javascript" src="/wp-content/themes/twentyten/js/masonry.min.js"></script>
<script>
    jQuery(window).load(function(){
        jQuery('.category-blog .Blog-Posts').masonry({
            itemSelector: '.Massonry',
            columnWidth: '.Massonry',
            percentPosition: true
        });
    });
</script>

</body>
</html>
