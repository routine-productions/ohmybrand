/*
 * Copyright (c) 2015
 * Routine JS - Image Align
 * Version 0.1.2
 * Create 2015.12.11
 * Author Bunker Labs
 */
(function($){
    $.fn.JS_Image_Align = function () {
        var $Images = $(this);
        $Images.find('img').css({
            'width': '100%',
            'position': 'relative'
        });

        $Images.each(function () {
            var Layout = $(this),
                Img = Layout.find('img'),
                Ratio = Layout.attr('data-image-ratio') ? $(this).attr('data-image-ratio') : false,
                Position = Layout.attr('data-image-position') ? $(this).attr('data-image-position') : 'center',
                Position_X = 'center',
                Position_Y = 'center';

            var Height = '';
            if (Ratio == 'auto') {
                Ratio = Img.actual('outerWidth') / Img.actual('outerHeight');
                Height = Layout.actual('outerWidth') / eval(Ratio);
            } else if (Ratio.length) {
                Height = Layout.actual('outerWidth') / eval(Ratio);
            } else {
                Height = Layout.actual('outerHeight');
            }

            if (Position != 'center') {
                Position = Position.split('/');
                Position_X = Position[0];
                Position_Y = Position[1];
            }

            Layout.css({
                'height': Height,
                'background-image': 'url(' + Img.attr('src') + ')',
                'background-size': 'cover',
                'background-position-x': Position_X,
                'background-position-y': Position_Y
            });

            Img.remove();

            $(window).resize(function () {
                Layout.css({
                    'height': Layout.outerWidth() / eval(Ratio)
                });
            });
        });
    };

    $(document).ready(function () {
        $('.Js-Slider .slides li ').JS_Image_Align();
    });
})(jQuery);



