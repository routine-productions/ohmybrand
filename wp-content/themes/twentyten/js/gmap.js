var map;
var kuus = new google.maps.LatLng(55.788040, 37.699951);
 
var MY_MAPTYPE_ID = 'mystyle';
 
function initialize() {
 
  var stylez =  [
  {
    featureType: "road",
    elementType: "geometry",
    stylers: [
      { visibility: "on" },
      { hue: "#333333" },
      { saturation: "-100" },
      { lightness: 10 }
    ]
  },{
    featureType: "water",
    elementType: "geometry",
    stylers: [
       { visibility: "om" },
      { hue: "#eaeaea" },
      { saturation: "-100" },
      { lightness: -10 }
    ]
  },{
    featureType: "poi.park",
    elementType: "geometry",
    stylers: [
      { visibility: "on" },
      { hue: "#cccccc" },
      { saturation: "-100" },
      { lightness: 20 }
    ]
  },{
    featureType: "administrative",
    elementType: "geometry",
    stylers: [
      { visibility: "on" },
      { hue: "#ffffff" },
      { saturation: "-100" },
      { lightness: 10 }
    ]
  },{
    featureType: "landscape",
    elementType: "geometry",
    stylers: [
      { visibility: "on" },
      { hue: "#FFFFFF" },
      { saturation: "-100" },
      { lightness: 10 }
    ]
  },{
    featureType: "road",
    elementType: "labels",
    stylers: [
      { visibility: "on" },
      { hue: "#FFFFFF" },
      { saturation: "-100" },
      { lightness: 10 }
    ]
  },{
    featureType: "all",
    elementType: "geometry",
    stylers: [
      { visibility: "on" },
      { hue: "#FFFFFF" },
      { saturation: "-100" },
      { lightness: 10 }
    ]
  }
  ,{
    featureType: "all",
    elementType: "labels",
    stylers: [
      { visibility: "on" },
      { hue: "#FFFFFF" },
      { saturation: "-100" },
      { lightness: 10 }
    ]
  }
];
  var zoom = "/wp-content/themes/twentyten/images/zoom.png";
  var mapOptions = {
    zoom: 16,
    center: kuus,
    mapTypeControlOptions: {
       mapTypeIds: [google.maps.MapTypeId.ROADMAP, MY_MAPTYPE_ID]
    },
    mapTypeId: MY_MAPTYPE_ID,
     navigationControl: true,
     disableDefaultUI: true,
     navigationControlOptions: {
        style: google.maps.NavigationControlStyle.SMALL,
        icon: zoom
    }
  };
  
 map = new google.maps.Map(document.getElementById("map_canvas"),
      mapOptions);
  
  var image = "/wp-content/themes/twentyten/images/marker.png";
  var myLatLng = new google.maps.LatLng(55.788040, 37.699951);
  var marker = new google.maps.Marker({
      position: myLatLng, 
      map: map, 
        icon: image
  });   
 

 
  var styledMapOptions = {
    name: "styled"
  };
 
  var jayzMapType = new google.maps.StyledMapType(stylez, styledMapOptions);
 
  map.mapTypes.set(MY_MAPTYPE_ID, jayzMapType);
}