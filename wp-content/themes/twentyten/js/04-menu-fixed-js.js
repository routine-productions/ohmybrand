/**
 * Created by routine-01 on 22.07.16.
 */

(function($){
    $(window).scroll(function ()  {

        if ($(window).scrollTop() > 50 ) {
            $('.Site-Header-Wrap').addClass('Menu-Fixed');
        }
        else {
            $('.Site-Header-Wrap').removeClass('Menu-Fixed');
        }
    });


    $(window).resize(function () {
        if ($(window).scrollTop() > 50) {
            $('.Site-Header-Wrap').addClass('Menu-Fixed');
        }
        else {
            $('.Site-Header-Wrap').removeClass('Menu-Fixed');
        }
    });
})(jQuery);

