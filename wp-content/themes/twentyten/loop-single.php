<?php
/**
 * The loop that displays a single post.
 *
 * The loop displays the posts and the post content. See
 * http://codex.wordpress.org/The_Loop to understand it and
 * http://codex.wordpress.org/Template_Tags to understand
 * the tags used in it.
 *
 * This can be overridden in child themes with loop-single.php.
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.2
 */
?>

<?php if (have_posts()) while (have_posts()) : the_post(); ?>

    <section class="Post-Single-Wrap" id="post-<?php the_ID(); ?>" <?php post_class(); ?> >

        <h1 class="Post-Single-Title">
            <span>
            <?php the_title(); ?>
            </span>
        </h1>



        <?php if (get_field("mainimage")) { ?>
            <div class="Post-Single-Image">
                <div style="background-image: url(<?= the_field("mainimage"); ?>)"></div>
            </div>
        <?php } ?>

        <?php if (get_field("slider")) { ?>
            <div class="Post-Single-Slider Js-Slider">
                <?= do_shortcode(get_field("slider")); ?>
            </div>
        <?php } ?>

        <div class="Post-Single">

            <?php if (get_locale() == 'ru_RU') { ?>
                <div class="Co-Authors">
                    <?php $authors = get_post_custom_values('authors');
                    echo $authors[0]; ?>
                </div>
            <?php } else { ?>
                <div class="Co-Authors">
                    <?php $authors_en = get_post_custom_values('authors_en');
                    echo $authors_en[0]; ?>
                </div>
            <?php } ?>
            <div class="Description-Wrap">
                <div class="Description">


                    <?php the_content(); ?>
                    <?php wp_link_pages(array('before' => '<div class="page-link">' . __('Pages:', 'twentyten'), 'after' => '</div>')); ?>
                    <div class="Post-Nav">
                        <div class="Navigation">
                            <div class="Prev"><?php previous_post_link('%link', '<span class="meta-nav">' . _x('&larr;', '', 'twentyten') . '</span> %title', TRUE, '10'); ?></div>
                            <div class="Next"><?php next_post_link('%link', '%title <span class="meta-nav">' . _x('&rarr;', 'Next post link', 'twentyten') . '</span>', TRUE); ?></div>
                        </div><!-- #nav-below -->
                    </div>
                </div>
            </div>



        </div>

        <?php if (get_the_author_meta('description')) : // If a user has filled out their description, show a bio on their entries  ?>
            <div id="entry-author-info Section-17">
                <div id="author-avatar">
                    <?php echo get_avatar(get_the_author_meta('user_email'), apply_filters('twentyten_author_bio_avatar_size', 60)); ?>
                </div>
                <!-- #author-avatar -->
                <div id="author-description">
                    <h2><?php printf(__('About %s', 'twentyten'), get_the_author()); ?></h2>
                    <?php the_author_meta('description'); ?>
                    <div id="author-link">
                        <a href="<?php echo get_author_posts_url(get_the_author_meta('ID')); ?>" rel="author">
                            <?php printf(__('View all posts by %s <span class="meta-nav">&rarr;</span>', 'twentyten'), get_the_author()); ?>
                        </a>
                    </div>
                    <!-- #author-link	-->
                </div>
                <!-- #author-description -->
            </div><!-- #entry-author-info -->
        <?php endif; ?>

        <div class="entry-utility Section-18">
            <?php twentyten_posted_in(); ?>
            <?php edit_post_link(__('Edit', 'twentyten'), '<span class="edit-link">', '</span>'); ?>
        </div>




    </section>



    <!-- #post-## -->



    <?php //comments_template( '', false ); ?>
<?php endwhile; // end of the loop. ?>
