<?php
/**
 * The Template for displaying all single posts.
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */

get_header(); ?>

		<div id="container Single-P">

			<div class="Single-Page" id="content" role="main">
      
			<?php
			/* Run the loop to output the post.
			 * If you want to overload this in a child theme then include a file
			 * called loop-single.php and that will be used instead.
			 */
			get_template_part( 'loop', 'single' );
			?>
			</div><!-- #content -->
				<div id="nav-below" class="navigation">
					<div class="nav-previous"><?php previous_post_link( '%link',  '<span class="meta-nav">' . _x( '&larr;', '', 'twentyten' ) . '</span> %title', TRUE, '10' ); ?></div>
					<div class="nav-next"><?php next_post_link( '%link', '%title <span class="meta-nav">' . _x( '&rarr;', 'Next post link', 'twentyten' ) . '</span>', TRUE ); ?></div>
				</div><!-- #nav-below -->    
    
		</div><!-- #container -->
    

<?php get_footer(); ?>
