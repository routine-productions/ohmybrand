<?php
/**
 * The Header widget area.
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */
?>

<?php
    /* The footer widget area is triggered if any of the areas
     * have widgets. So let's check that first.
     *
     * If none of the sidebars have widgets, then let's bail early.
     */
    if (   ! is_active_sidebar( 'heads-widget-area'  )
    )
        return;
    // If we get this far, we have widgets. Let do this.
?>
            
<?php if ( is_active_sidebar( 'heads-widget-area' ) ) : ?>
                <ul id="head_slogans" class="Header-Submenu">
                        <?php dynamic_sidebar( 'heads-widget-area' ); ?>
                </ul>
<?php endif; ?>