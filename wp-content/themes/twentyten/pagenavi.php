<?php
global $wp_query;
$max_page = $wp_query->max_num_pages;
$nump=6;  /*Количество отображаемых подряд номеров страниц*/
 
if($max_page>1){
    $paged = intval(get_query_var('paged'));
    if(empty($paged) || $paged == 0) $paged = Первая;
 
    echo '<div class="wp-pagenavi">';
/*  echo 'Страница '.$paged.' из '.$max_page.'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';*/
    if($paged!=1) echo '<a href="'.get_pagenum_link(1).'">&laquo;first</a>&nbsp;';
        else echo '<u>1</u>';
 
    if($paged-$nump>1) $start=$paged-$nump; else $start=2;
    if($paged+$nump<$max_page) $end=$paged+$nump; else $end=$max_page-1;
 
    if($start>2) echo "<b>...</b>";
 
    for ($i=$start;$i<=$end;$i++)
     {
     if($paged!=$i) echo '<a href="'.get_pagenum_link($i).'">'.$i.'</a> ';
        else echo '<u>'.$i.'</u>';
     }
 
    if($end<$max_page-1) echo "<b>...</b>";
 
    if($paged!=$max_page) echo '<a href="'.get_pagenum_link($max_page).'">&raquo;last</a>';
        else echo '<b>&nbsp;&raquo;</b> ';
    echo '</div>' ;
    }