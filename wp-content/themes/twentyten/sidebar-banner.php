<?php
/**
 * The Header widget area.
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */
?>

<?php
    /* The footer widget area is triggered if any of the areas
     * have widgets. So let's check that first.
     *
     * If none of the sidebars have widgets, then let's bail early.
     */
    if (   ! is_active_sidebar( 'header-widget-area'  )
    )
        return;
    // If we get this far, we have widgets. Let do this.
?>

            <div id="header-widget-area Header-Widgets" role="complementary">

<?php if ( is_active_sidebar( 'banner-widget-area' ) ) : ?>
                <div id="first" class="widget-area widget-area-1">
                        <?php dynamic_sidebar( 'banner-widget-area' ); ?>
                </div><!-- #banner .widget-area -->
<?php endif; ?>

            </div><!-- #banner-widget-area -->
